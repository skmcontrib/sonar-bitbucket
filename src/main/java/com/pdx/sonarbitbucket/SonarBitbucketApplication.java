package com.pdx.sonarbitbucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SonarBitbucketApplication {

	public static void main(String[] args) {
		SpringApplication.run(SonarBitbucketApplication.class, args);
	}

}
